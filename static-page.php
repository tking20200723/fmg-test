<?php
include 'includes/config.php';
?>

<html lang='en'>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Static data | Sample Project</title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/my.css">
</head>
<body>
	<?php include('includes/nav.php'); ?>
	<div class="my-container container">
		<p>
			<?php
			/* Check for logged in user */
			if ($loggedIn) {
				/* Retrieve user info from cookie and display */
				$userInfo = explode(',',urldecode($_COOKIE["fridayMedia"]));
				echo 'Welcome ' . $userInfo[1] . ' (' . $userInfo[0] . '),';
			}
			?>
		</p>

		<p>
			Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Venenatis cras sed felis eget velit aliquet sagittis id. Bibendum enim facilisis gravida neque. Accumsan tortor posuere ac ut consequat semper viverra. Felis bibendum ut tristique et egestas quis ipsum. Non nisi est sit amet facilisis magna etiam tempor. Nam aliquam sem et tortor consequat. Nunc mi ipsum faucibus vitae aliquet nec ullamcorper sit amet. Tortor at auctor urna nunc id cursus metus aliquam. Nulla facilisi nullam vehicula ipsum a arcu cursus. Commodo ullamcorper a lacus vestibulum sed. Cursus euismod quis viverra nibh cras pulvinar mattis nunc. Et malesuada fames ac turpis egestas maecenas. Euismod elementum nisi quis eleifend. Luctus accumsan tortor posuere ac ut consequat semper viverra nam. Massa enim nec dui nunc mattis enim. Neque ornare aenean euismod elementum. Elementum pulvinar etiam non quam lacus suspendisse faucibus. Id venenatis a condimentum vitae sapien pellentesque. Nam libero justo laoreet sit amet cursus.
		</p>
		<p>
			Nibh mauris cursus mattis molestie. Hendrerit gravida rutrum quisque non tellus. Risus nullam eget felis eget nunc. Diam vulputate ut pharetra sit amet aliquam id diam maecenas. Eu nisl nunc mi ipsum faucibus vitae aliquet. Cursus metus aliquam eleifend mi. Turpis egestas sed tempus urna et pharetra pharetra. Volutpat ac tincidunt vitae semper quis. Dui faucibus in ornare quam viverra orci. Vel risus commodo viverra maecenas accumsan lacus vel facilisis volutpat. Mi ipsum faucibus vitae aliquet nec ullamcorper sit amet risus. Tempor nec feugiat nisl pretium. Mauris pellentesque pulvinar pellentesque habitant. Magna ac placerat vestibulum lectus mauris ultrices eros. Felis imperdiet proin fermentum leo vel. Pellentesque nec nam aliquam sem et. Suspendisse faucibus interdum posuere lorem ipsum dolor sit amet. Morbi quis commodo odio aenean.
		</p>
		<p>
			Quis auctor elit sed vulputate mi. Ullamcorper sit amet risus nullam eget felis eget. Nam libero justo laoreet sit. Aliquam malesuada bibendum arcu vitae elementum curabitur vitae. Pulvinar elementum integer enim neque. Mauris cursus mattis molestie a iaculis at erat. Tellus molestie nunc non blandit massa enim nec. Euismod nisi porta lorem mollis aliquam ut porttitor leo a. Quis imperdiet massa tincidunt nunc pulvinar sapien. Scelerisque eleifend donec pretium vulputate sapien nec sagittis aliquam malesuada. Sit amet consectetur adipiscing elit. Lacinia at quis risus sed. Adipiscing enim eu turpis egestas. At quis risus sed vulputate odio ut enim blandit. Lectus arcu bibendum at varius vel. Elementum curabitur vitae nunc sed velit.
		</p>
	</div>
	<br>
	<footer class="bg-dark" style="height: 30px;">
	</footer>
</body>
<script src="assets/js/jquery-3.6.0.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
</html>