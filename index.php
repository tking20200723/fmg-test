<?php
include 'includes/config.php';

?>

<html lang='en'>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Home | Sample Project</title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/my.css">
</head>
<body>
	<?php include('includes/nav.php'); ?>
	<div class="my-container container text-center">
		<h1 class="">Home</h1>

		<div id="demoCarousel" class="carousel slide" data-ride="carousel">

		  <!-- Indicators -->
		  <ul class="carousel-indicators">
		    <li data-target="#demoCarousel" data-slide-to="0" class="active"></li>
		    <li data-target="#demoCarousel" data-slide-to="1"></li>
		    <li data-target="#demoCarousel" data-slide-to="2"></li>
		  </ul>

		  <!-- The slideshow -->
		  <div class="carousel-inner">
		    <div class="carousel-item active">
		      <img src="assets/img/img_01.jpg" alt="Berries">
		    </div>
		    <div class="carousel-item">
		      <img src="assets/img/img_02.jpg" alt="Robin">
		    </div>
		    <div class="carousel-item">
		      <img src="assets/img/img_03.jpg" alt="House">
		    </div>
		  </div>

		  <!-- Left and right controls -->
		  <a class="carousel-control-prev" href="#demoCarousel" data-slide="prev">
		    <span class="carousel-control-prev-icon"></span>
		  </a>
		  <a class="carousel-control-next" href="#demoCarousel" data-slide="next">
		    <span class="carousel-control-next-icon"></span>
		  </a>
		</div>
	</div>
	<footer class="bg-dark" style="height: 30px;">
	</footer>
</body>
<script src="assets/js/jquery-3.6.0.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
</html>

