<?php
/* */
include 'includes/db.php';
include 'includes/config.php';

/* Not logged in? */
if (!$loggedIn) {
	header('Location: login.php?access=denied');
} else {

	/* Check for filters */
	$cityFilter = isset($_GET['city']) ? trim(mysqli_real_escape_string($open_db, $_GET['city'])) : '';
	$productFilter = isset($_GET['product']) ? trim(mysqli_real_escape_string($open_db, $_GET['product'])) : '';

	/* Build query filter */
	$filter = ($cityFilter != '')? "city = '" . $cityFilter . "'" : '';
	$filter .= $filter != '' && $productFilter != '' ? ' AND ' : '';
	$filter .= $productFilter != '' ? "product = '" . $productFilter . "'" : '';
	$filter = ($filter != '') ? 'WHERE ' . $filter : '';

	/* Build query and execute */
	$qry = "SELECT region, city, category, product, quantity, unit_price, total_price FROM food_orders $filter";
	$sales_query = $open_db->query($qry);

	/* Calculate pages based on 10 items/page */
	$totalPages = intval(ceil($sales_query->num_rows / 10));
}

?>

<html lang='en'>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Sales data | Sample Project</title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/my.css">
</head>
<body>
	<?php include('includes/nav.php'); ?>

	<div class="my-container container text-center">
		<h1 class="">Sales Info</h1>
		<form name="search" method="get">
			<div class="row">
				<div class="col-lg-5">
					<label>City</label>
					<input name="city" type="text" class="form-control" value="<?=$cityFilter; ?>">
				</div>
				<div class="col-lg-5">
					<label>Product</label>
					<input name="product" type="text" class="form-control" value="<?=$productFilter; ?>">
				</div>
				<div class="col-lg-1">
					<input type="submit" value="Filter" class="form-control" style="margin-top: 32px;">
				</div>
				<div class="col-lg-1">
					<a href="restricted.php" class="form-control" style="text-decoration: none;margin-top: 32px;">Clear</a>
				</div>
			</div>
		</form>
		<?php
		/* Loop for calculated number of pages */
		for ($pg=1; $pg<=$totalPages; $pg++) {
		?>
		<div id="pg-<?=$pg; ?>" <?=($pg != 1 ? 'style="visibility: hidden; display:none;"' : 'style="visibility: visible; display: default;"'); ?>>
			<table class="table table-striped">
				<thead>
					<th>Region</th>
					<th>City</th>
					<th>Category</th>
					<th>Product</th>
					<th>Quantity</th>
					<th>Unit_price</th>
					<th>Total_price</th>
				</thead>
				<tbody>
					<?php
					/* Display 10 rows */
					for ($i=1; $i<=10; $i++) {
						$record = mysqli_fetch_assoc($sales_query);
						if ($record) {
						?>
						<tr>
							<td><?=$record['region']; ?></td>
							<td><?=$record['city']; ?></td>
							<td><?=$record['category']; ?></td>
							<td><?=$record['product']; ?></td>
							<td><?=$record['quantity']; ?></td>
							<td><?=$record['unit_price']; ?></td>
							<td><?=$record['total_price']; ?></td>
						</tr>
						<?php
						}
					}
					?>
				</tbody>
			</table>
		</div>
		<?php
		}
		?>
	</div>


	<div class="text-center">
		<button onclick="prevPg()">Prev</button>
		<select id="pgSelect" onchange="selectPg(this.value)">
		<?php
		/* Loop for calculated number of pages */
		for ($pg=1; $pg<=$totalPages; $pg++) {
		?>
			<option value=<?=$pg; ?>><?=$pg; ?></option>
		<?php
		}
		?>
		</select>
		<button onclick="nextPg()">Next</button>
	</div>
	<br><br>
	<footer class="bg-dark" style="height: 30px;">
	</footer>
</body>
<script src="assets/js/jquery-3.6.0.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script>

	/* Get global variables for page */
	var totalPages = <?=$totalPages; ?>;
	var currentPage = 1;

	/* Previous page function */
	function prevPg() {
		if (currentPage != 1) {
			document.getElementById('pg-'+currentPage).style.visibility = 'hidden';
			document.getElementById('pg-'+currentPage).style.display = 'none';
			currentPage--;
			document.getElementById('pg-'+currentPage).style.visibility = 'visible';
			document.getElementById('pg-'+currentPage).style.display = 'initial';
			document.getElementById('pgSelect').value = currentPage;
		}
	}

	/* Page select function */
	function selectPg(newPg) {
			document.getElementById('pg-'+currentPage).style.visibility = 'hidden';
			document.getElementById('pg-'+currentPage).style.display = 'none';
			currentPage = newPg;
			document.getElementById('pg-'+currentPage).style.visibility = 'visible';
			document.getElementById('pg-'+currentPage).style.display = 'initial';
	}

	/* Next page function */
	function nextPg() {
		if (currentPage != totalPages) {
			document.getElementById('pg-'+currentPage).style.visibility = 'hidden';
			document.getElementById('pg-'+currentPage).style.display = 'none';
			currentPage++;
			document.getElementById('pg-'+currentPage).style.visibility = 'visible';
			document.getElementById('pg-'+currentPage).style.display = 'initial';
			document.getElementById('pgSelect').value = currentPage;
		}
	}

</script>
</html>