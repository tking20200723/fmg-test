<?php

include 'includes/db.php';
include 'includes/config.php';

	/* Logout */
if (isset($_GET['logout']) && $_GET['logout'] == 'true') {

	/* Destroy cookie */
	setcookie("fridayMedia", 'logged-in', time() - 3600, "/");
	header('Location: login.php');
}

/* login */
if (isset($_POST['login'])) {

	/* Get login details */
	$email = mysqli_real_escape_string($open_db, $_POST['email']);
	$password = md5(mysqli_real_escape_string($open_db, $_POST['password']));

	/* Build query */
	$qry = "SELECT email, name FROM users WHERE email = '$email' AND password = '$password'";
	/* Execute query retrieve first row */
	$user_query = $open_db->query($qry);

	/* Check that a row was returned */

	if ($user_query->num_rows != 0) {

		/* Retrieve user data */
		$user_data = mysqli_fetch_assoc($user_query);
		/* Store data in a local cookie */
		setcookie("fridayMedia", urlencode($user_data['email']) . ',' . $user_data['name'], time() + 3600, "/");
		/* Re-direct to restricted page */
		header('Location: restricted.php');
	} else {
		/* Re-load page highlighting failed attempt */
		header('Location: login.php?invalid');
	}

/* Register */

} elseif (isset($_POST['register'])) {

	/* Get registration details */
	$email = mysqli_real_escape_string($open_db, $_POST['email']);
	$name = mysqli_real_escape_string($open_db, $_POST['name']);
	$password = md5(mysqli_real_escape_string($open_db, $_POST['password']));

	/* Build query and create new user record */
	$ins_qry = "INSERT INTO users (email, name, password) VALUES ('$email', '$name', '$password')";
	$open_db->query($ins_qry) or die(mysqli_error($open_db));
	header('Location: login.php?registered');
}

?>
<html lang='en'>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login / Registration | Sample Project</title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/my.css">
</head>
<body>
	<?php include('includes/nav.php'); ?>
	<div class="my-container container">
		<?php
		/* Access denied */
		if (isset($_GET['access']) && $_GET['access'] == 'denied') {
		?>
			<div class="alert alert-warning">
			  <strong>Access denied!</strong> Please login.
			</div>
		<?php
		}

		/* Failed login */
		if (isset($_GET['invalid'])) {
		?>
			<div class="alert alert-danger">
			  <strong>Invalid login attempt!</strong> Please try again.
			</div>
		<?php
		}

		/* New registration */
		if (isset($_GET['registered'])) {
		?>
			<div class="alert alert-success">
			  <strong>You are now registered</strong> Please login.
			</div>
		<?php
		}
		?>
		<div class="row">
			<div class="col-lg-6">
				<div class="card">
					<div class="card-header text-center"><strong>Login</strong></div>
					<div class="card-body">
						<form name="login" method="post">
							<div class="form-group">
								<label>Email</label>
								<input name="email" type="email" required="required" placeholder="i.e. log-me-in@here.com" class="form-control">
							</div>
							<div class="form-group">
								<label>Password</label>
								<input name="password" type="password" required="required" class="form-control">
							</div>
							<div class="form-group">
								<input type="submit" name="login" value="Login">
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="card">
					<div class="card-header text-center"><strong>Register</strong></div>
					<div class="card-body">
						<form name="login" method="post">
							<div class="form-group">
								<label>Email</label>
								<input name="email" type="email" required="required" placeholder="i.e. register-me@here.com" class="form-control">
							</div>
							<div class="form-group">
								<label>Name</label>
								<input name="name" type="name" required="required" placeholder="i.e. job bloggs" class="form-control">
							</div>
							<div class="form-group">
								<label>Password</label>
								<input name="password" type="password" required="required" class="form-control">
							</div>
							<div class="form-group">
								<input type="submit" name="register" value="Register">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br>
	<footer class="bg-dark" style="height: 30px;">
	</footer>
</body>
<script src="assets/js/jquery-3.6.0.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
</html>
