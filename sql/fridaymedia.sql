-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 07, 2021 at 12:48 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fridaymedia`
--

-- --------------------------------------------------------

--
-- Table structure for table `food_orders`
--

CREATE TABLE `food_orders` (
  `id` int(11) NOT NULL,
  `region` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `product` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_price` float NOT NULL,
  `total_price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `food_orders`
--

INSERT INTO `food_orders` (`id`, `region`, `city`, `category`, `product`, `quantity`, `unit_price`, `total_price`) VALUES
(2, 'East', 'Boston', 'Bars', 'Carrot', 33, 1.77, 58.41),
(3, 'East', 'Boston', 'Crackers', 'Whole Wheat', 87, 3.49, 303.63),
(4, 'West', 'Los Angeles', 'Cookies', 'Chocolate Chip', 58, 1.87, 108.46),
(5, 'East', 'New York', 'Cookies', 'Chocolate Chip', 82, 1.87, 153.34),
(6, 'East', 'Boston', 'Cookies', 'Arrowroot', 38, 2.18, 82.84),
(7, 'East', 'Boston', 'Bars', 'Carrot', 54, 1.77, 95.58),
(8, 'East', 'Boston', 'Crackers', 'Whole Wheat', 149, 3.49, 520.01),
(9, 'West', 'Los Angeles', 'Bars', 'Carrot', 51, 1.77, 90.27),
(10, 'East', 'New York', 'Bars', 'Carrot', 100, 1.77, 177),
(11, 'East', 'New York', 'Snacks', 'Potato Chips', 28, 1.35, 37.8),
(12, 'East', 'Boston', 'Cookies', 'Arrowroot', 36, 2.18, 78.48),
(13, 'East', 'Boston', 'Cookies', 'Chocolate Chip', 31, 1.87, 57.97),
(14, 'East', 'Boston', 'Crackers', 'Whole Wheat', 28, 3.49, 97.72),
(15, 'West', 'Los Angeles', 'Bars', 'Carrot', 44, 1.77, 77.88),
(16, 'East', 'New York', 'Bars', 'Carrot', 23, 1.77, 40.71),
(17, 'East', 'New York', 'Snacks', 'Potato Chips', 27, 1.35, 36.45),
(18, 'East', 'Boston', 'Cookies', 'Arrowroot', 43, 2.18, 93.74),
(19, 'East', 'Boston', 'Cookies', 'Oatmeal Raisin', 123, 2.84, 349.32),
(20, 'West', 'Los Angeles', 'Bars', 'Bran', 42, 1.87, 78.54),
(21, 'West', 'Los Angeles', 'Cookies', 'Oatmeal Raisin', 33, 2.84, 93.72),
(22, 'East', 'New York', 'Cookies', 'Chocolate Chip', 85, 1.87, 158.95),
(23, 'West', 'San Diego', 'Cookies', 'Oatmeal Raisin', 30, 2.84, 85.2),
(24, 'East', 'Boston', 'Bars', 'Carrot', 61, 1.77, 107.97),
(25, 'East', 'Boston', 'Crackers', 'Whole Wheat', 40, 3.49, 139.6),
(26, 'West', 'Los Angeles', 'Cookies', 'Chocolate Chip', 86, 1.87, 160.82),
(27, 'East', 'New York', 'Bars', 'Carrot', 38, 1.77, 67.26),
(28, 'East', 'New York', 'Snacks', 'Potato Chips', 68, 1.68, 114.24),
(29, 'West', 'San Diego', 'Cookies', 'Chocolate Chip', 39, 1.87, 72.93),
(30, 'East', 'Boston', 'Bars', 'Bran', 103, 1.87, 192.61),
(31, 'East', 'Boston', 'Cookies', 'Oatmeal Raisin', 193, 2.84, 548.12),
(32, 'West', 'Los Angeles', 'Bars', 'Carrot', 58, 1.77, 102.66),
(33, 'West', 'Los Angeles', 'Snacks', 'Potato Chips', 68, 1.68, 114.24),
(34, 'East', 'New York', 'Bars', 'Carrot', 91, 1.77, 161.07),
(35, 'East', 'New York', 'Crackers', 'Whole Wheat', 23, 3.49, 80.27),
(36, 'West', 'San Diego', 'Snacks', 'Potato Chips', 28, 1.68, 47.04),
(37, 'East', 'Boston', 'Bars', 'Carrot', 48, 1.77, 84.96),
(38, 'East', 'Boston', 'Snacks', 'Potato Chips', 134, 1.68, 225.12),
(39, 'West', 'Los Angeles', 'Bars', 'Carrot', 20, 1.77, 35.4),
(40, 'East', 'New York', 'Bars', 'Carrot', 53, 1.77, 93.81),
(41, 'East', 'New York', 'Snacks', 'Potato Chips', 64, 1.68, 107.52),
(42, 'West', 'San Diego', 'Cookies', 'Chocolate Chip', 63, 1.87, 117.81),
(43, 'East', 'Boston', 'Bars', 'Bran', 105, 1.87, 196.35),
(44, 'East', 'Boston', 'Cookies', 'Oatmeal Raisin', 138, 2.84, 391.92),
(45, 'West', 'Los Angeles', 'Bars', 'Carrot', 25, 1.77, 44.25),
(46, 'West', 'Los Angeles', 'Crackers', 'Whole Wheat', 21, 3.49, 73.29),
(47, 'East', 'New York', 'Bars', 'Carrot', 61, 1.77, 107.97),
(48, 'East', 'New York', 'Snacks', 'Potato Chips', 49, 1.68, 82.32),
(49, 'West', 'San Diego', 'Cookies', 'Chocolate Chip', 55, 1.87, 102.85),
(50, 'East', 'Boston', 'Cookies', 'Arrowroot', 27, 2.18, 58.86),
(51, 'East', 'Boston', 'Bars', 'Carrot', 58, 1.77, 102.66),
(52, 'East', 'Boston', 'Crackers', 'Whole Wheat', 33, 3.49, 115.17),
(53, 'West', 'Los Angeles', 'Cookies', 'Oatmeal Raisin', 288, 2.84, 817.92),
(54, 'East', 'New York', 'Cookies', 'Chocolate Chip', 76, 1.87, 142.12),
(55, 'West', 'San Diego', 'Bars', 'Carrot', 42, 1.77, 74.34),
(56, 'West', 'San Diego', 'Crackers', 'Whole Wheat', 20, 3.49, 69.8),
(57, 'East', 'Boston', 'Bars', 'Carrot', 75, 1.77, 132.75),
(58, 'East', 'Boston', 'Crackers', 'Whole Wheat', 38, 3.49, 132.62),
(59, 'West', 'Los Angeles', 'Bars', 'Carrot', 306, 1.77, 541.62),
(60, 'West', 'Los Angeles', 'Snacks', 'Potato Chips', 28, 1.68, 47.04),
(61, 'East', 'New York', 'Bars', 'Bran', 110, 1.87, 205.7),
(62, 'East', 'New York', 'Cookies', 'Oatmeal Raisin', 51, 2.84, 144.84),
(63, 'West', 'San Diego', 'Bars', 'Carrot', 52, 1.77, 92.04),
(64, 'West', 'San Diego', 'Crackers', 'Whole Wheat', 28, 3.49, 97.72),
(65, 'East', 'Boston', 'Bars', 'Carrot', 136, 1.77, 240.72),
(66, 'East', 'Boston', 'Crackers', 'Whole Wheat', 42, 3.49, 146.58),
(67, 'West', 'Los Angeles', 'Cookies', 'Chocolate Chip', 75, 1.87, 140.25),
(68, 'East', 'New York', 'Bars', 'Bran', 72, 1.87, 134.64),
(69, 'East', 'New York', 'Cookies', 'Oatmeal Raisin', 56, 2.84, 159.04),
(70, 'West', 'San Diego', 'Bars', 'Bran', 51, 1.87, 95.37),
(71, 'West', 'San Diego', 'Snacks', 'Potato Chips', 31, 1.68, 52.08),
(72, 'East', 'Boston', 'Bars', 'Bran', 56, 1.87, 104.72),
(73, 'East', 'Boston', 'Cookies', 'Oatmeal Raisin', 137, 2.84, 389.08),
(74, 'West', 'Los Angeles', 'Cookies', 'Chocolate Chip', 107, 1.87, 200.09),
(75, 'East', 'New York', 'Bars', 'Carrot', 24, 1.77, 42.48),
(76, 'East', 'New York', 'Crackers', 'Whole Wheat', 30, 3.49, 104.7),
(77, 'West', 'San Diego', 'Cookies', 'Chocolate Chip', 70, 1.87, 130.9),
(78, 'East', 'Boston', 'Cookies', 'Arrowroot', 31, 2.18, 67.58),
(79, 'East', 'Boston', 'Bars', 'Carrot', 109, 1.77, 192.93),
(80, 'East', 'Boston', 'Crackers', 'Whole Wheat', 21, 3.49, 73.29),
(81, 'West', 'Los Angeles', 'Cookies', 'Chocolate Chip', 80, 1.87, 149.6),
(82, 'East', 'New York', 'Bars', 'Bran', 75, 1.87, 140.25),
(83, 'East', 'New York', 'Cookies', 'Oatmeal Raisin', 74, 2.84, 210.16),
(84, 'West', 'San Diego', 'Bars', 'Carrot', 45, 1.77, 79.65),
(85, 'East', 'Boston', 'Cookies', 'Arrowroot', 28, 2.18, 61.04),
(86, 'East', 'Boston', 'Bars', 'Carrot', 143, 1.77, 253.11),
(87, 'East', 'Boston', 'Snacks', 'Pretzels', 27, 3.15, 85.05),
(88, 'West', 'Los Angeles', 'Bars', 'Carrot', 133, 1.77, 235.41),
(89, 'East', 'New York', 'Cookies', 'Arrowroot', 110, 2.18, 239.8),
(90, 'East', 'New York', 'Cookies', 'Chocolate Chip', 65, 1.87, 121.55),
(91, 'West', 'San Diego', 'Bars', 'Bran', 33, 1.87, 61.71),
(92, 'East', 'Boston', 'Cookies', 'Arrowroot', 81, 2.18, 176.58),
(93, 'East', 'Boston', 'Bars', 'Carrot', 77, 1.77, 136.29),
(94, 'East', 'Boston', 'Crackers', 'Whole Wheat', 38, 3.49, 132.62),
(95, 'West', 'Los Angeles', 'Bars', 'Carrot', 40, 1.77, 70.8),
(96, 'West', 'Los Angeles', 'Snacks', 'Potato Chips', 114, 1.68, 191.52),
(97, 'East', 'New York', 'Cookies', 'Arrowroot', 224, 2.18, 488.32),
(98, 'East', 'New York', 'Bars', 'Carrot', 141, 1.77, 249.57),
(99, 'East', 'New York', 'Crackers', 'Whole Wheat', 32, 3.49, 111.68),
(100, 'West', 'San Diego', 'Bars', 'Carrot', 20, 1.77, 35.4),
(101, 'East', 'Boston', 'Cookies', 'Arrowroot', 40, 2.18, 87.2),
(102, 'East', 'Boston', 'Cookies', 'Chocolate Chip', 49, 1.87, 91.63),
(103, 'East', 'Boston', 'Crackers', 'Whole Wheat', 46, 3.49, 160.54),
(104, 'West', 'Los Angeles', 'Bars', 'Carrot', 39, 1.77, 69.03),
(105, 'West', 'Los Angeles', 'Snacks', 'Potato Chips', 62, 1.68, 104.16),
(106, 'East', 'New York', 'Bars', 'Carrot', 90, 1.77, 159.3),
(107, 'West', 'San Diego', 'Cookies', 'Arrowroot', 103, 2.18, 224.54),
(108, 'West', 'San Diego', 'Cookies', 'Oatmeal Raisin', 32, 2.84, 90.88),
(109, 'East', 'Boston', 'Bars', 'Bran', 66, 1.87, 123.42),
(110, 'East', 'Boston', 'Cookies', 'Oatmeal Raisin', 97, 2.84, 275.48),
(111, 'West', 'Los Angeles', 'Bars', 'Carrot', 30, 1.77, 53.1),
(112, 'West', 'Los Angeles', 'Snacks', 'Potato Chips', 29, 1.68, 48.72),
(113, 'East', 'New York', 'Bars', 'Carrot', 92, 1.77, 162.84),
(114, 'West', 'San Diego', 'Cookies', 'Arrowroot', 139, 2.18, 303.02),
(115, 'West', 'San Diego', 'Cookies', 'Oatmeal Raisin', 29, 2.84, 82.36),
(116, 'East', 'Boston', 'Bars', 'Banana', 30, 2.27, 68.1),
(117, 'East', 'Boston', 'Cookies', 'Chocolate Chip', 36, 1.87, 67.32),
(118, 'East', 'Boston', 'Crackers', 'Whole Wheat', 41, 3.49, 143.09),
(119, 'West', 'Los Angeles', 'Bars', 'Carrot', 44, 1.77, 77.88),
(120, 'West', 'Los Angeles', 'Snacks', 'Potato Chips', 29, 1.68, 48.72),
(121, 'East', 'New York', 'Cookies', 'Arrowroot', 237, 2.18, 516.66),
(122, 'East', 'New York', 'Cookies', 'Chocolate Chip', 65, 1.87, 121.55),
(123, 'West', 'San Diego', 'Cookies', 'Arrowroot', 83, 2.18, 180.94),
(124, 'East', 'Boston', 'Cookies', 'Arrowroot', 32, 2.18, 69.76),
(125, 'East', 'Boston', 'Bars', 'Carrot', 63, 1.77, 111.51),
(126, 'East', 'Boston', 'Snacks', 'Pretzels', 29, 3.15, 91.35),
(127, 'West', 'Los Angeles', 'Bars', 'Bran', 77, 1.87, 143.99),
(128, 'West', 'Los Angeles', 'Cookies', 'Oatmeal Raisin', 80, 2.84, 227.2),
(129, 'East', 'New York', 'Bars', 'Carrot', 102, 1.77, 180.54),
(130, 'East', 'New York', 'Crackers', 'Whole Wheat', 31, 3.49, 108.19),
(131, 'West', 'San Diego', 'Bars', 'Carrot', 56, 1.77, 99.12),
(132, 'East', 'Boston', 'Cookies', 'Arrowroot', 52, 2.18, 113.36),
(133, 'East', 'Boston', 'Bars', 'Carrot', 51, 1.77, 90.27),
(134, 'East', 'Boston', 'Snacks', 'Potato Chips', 24, 1.68, 40.32),
(135, 'West', 'Los Angeles', 'Cookies', 'Arrowroot', 58, 2.18, 126.44),
(136, 'West', 'Los Angeles', 'Cookies', 'Chocolate Chip', 34, 1.87, 63.58),
(137, 'East', 'New York', 'Bars', 'Carrot', 34, 1.77, 60.18),
(138, 'East', 'New York', 'Snacks', 'Potato Chips', 21, 1.68, 35.28),
(139, 'West', 'San Diego', 'Cookies', 'Oatmeal Raisin', 29, 2.84, 82.36),
(140, 'East', 'Boston', 'Bars', 'Carrot', 68, 1.77, 120.36),
(141, 'East', 'Boston', 'Snacks', 'Pretzels', 31, 3.15, 97.65),
(142, 'West', 'Los Angeles', 'Cookies', 'Arrowroot', 30, 2.18, 65.4),
(143, 'West', 'Los Angeles', 'Cookies', 'Chocolate Chip', 232, 1.87, 433.84),
(144, 'East', 'New York', 'Bars', 'Bran', 68, 1.87, 127.16),
(145, 'East', 'New York', 'Cookies', 'Oatmeal Raisin', 97, 2.84, 275.48),
(146, 'West', 'San Diego', 'Bars', 'Bran', 86, 1.87, 160.82),
(147, 'West', 'San Diego', 'Snacks', 'Potato Chips', 41, 1.68, 68.88),
(148, 'East', 'Boston', 'Bars', 'Carrot', 93, 1.77, 164.61),
(149, 'East', 'Boston', 'Snacks', 'Potato Chips', 47, 1.68, 78.96),
(150, 'West', 'Los Angeles', 'Bars', 'Carrot', 103, 1.77, 182.31),
(151, 'West', 'Los Angeles', 'Snacks', 'Potato Chips', 33, 1.68, 55.44),
(152, 'East', 'New York', 'Bars', 'Bran', 57, 1.87, 106.59),
(153, 'East', 'New York', 'Cookies', 'Oatmeal Raisin', 65, 2.84, 184.6),
(154, 'West', 'San Diego', 'Bars', 'Carrot', 118, 1.77, 208.86),
(155, 'East', 'Boston', 'Cookies', 'Arrowroot', 36, 2.18, 78.48),
(156, 'East', 'Boston', 'Cookies', 'Oatmeal Raisin', 123, 2.84, 349.32),
(157, 'West', 'Los Angeles', 'Bars', 'Carrot', 90, 1.77, 159.3),
(158, 'West', 'Los Angeles', 'Crackers', 'Whole Wheat', 21, 3.49, 73.29),
(159, 'East', 'New York', 'Bars', 'Carrot', 48, 1.77, 84.96),
(160, 'East', 'New York', 'Snacks', 'Potato Chips', 24, 1.68, 40.32),
(161, 'West', 'San Diego', 'Cookies', 'Chocolate Chip', 67, 1.87, 125.29),
(162, 'East', 'Boston', 'Bars', 'Bran', 27, 1.87, 50.49),
(163, 'East', 'Boston', 'Cookies', 'Oatmeal Raisin', 129, 2.84, 366.36),
(164, 'West', 'Los Angeles', 'Cookies', 'Arrowroot', 77, 2.18, 167.86),
(165, 'West', 'Los Angeles', 'Cookies', 'Chocolate Chip', 58, 1.87, 108.46),
(166, 'East', 'New York', 'Bars', 'Bran', 47, 1.87, 87.89),
(167, 'East', 'New York', 'Cookies', 'Oatmeal Raisin', 33, 2.84, 93.72),
(168, 'West', 'San Diego', 'Cookies', 'Chocolate Chip', 82, 1.87, 153.34),
(169, 'East', 'Boston', 'Bars', 'Carrot', 58, 1.77, 102.66),
(170, 'East', 'Boston', 'Snacks', 'Pretzels', 30, 3.15, 94.5),
(171, 'West', 'Los Angeles', 'Cookies', 'Chocolate Chip', 43, 1.87, 80.41),
(172, 'East', 'New York', 'Bars', 'Carrot', 84, 1.77, 148.68),
(173, 'West', 'San Diego', 'Cookies', 'Arrowroot', 36, 2.18, 78.48),
(174, 'West', 'San Diego', 'Cookies', 'Oatmeal Raisin', 44, 2.84, 124.96),
(175, 'East', 'Boston', 'Bars', 'Bran', 27, 1.87, 50.49),
(176, 'East', 'Boston', 'Cookies', 'Oatmeal Raisin', 120, 2.84, 340.8),
(177, 'East', 'Boston', 'Crackers', 'Whole Wheat', 26, 3.49, 90.74),
(178, 'West', 'Los Angeles', 'Bars', 'Carrot', 73, 1.77, 129.21),
(179, 'East', 'New York', 'Bars', 'Bran', 38, 1.87, 71.06),
(180, 'East', 'New York', 'Cookies', 'Oatmeal Raisin', 40, 2.84, 113.6),
(181, 'West', 'San Diego', 'Bars', 'Carrot', 41, 1.77, 72.57),
(182, 'East', 'Boston', 'Bars', 'Banana', 27, 2.27, 61.29),
(183, 'East', 'Boston', 'Cookies', 'Chocolate Chip', 38, 1.87, 71.06),
(184, 'East', 'Boston', 'Crackers', 'Whole Wheat', 34, 3.49, 118.66),
(185, 'West', 'Los Angeles', 'Bars', 'Bran', 65, 1.87, 121.55),
(186, 'West', 'Los Angeles', 'Cookies', 'Oatmeal Raisin', 60, 2.84, 170.4),
(187, 'East', 'New York', 'Cookies', 'Arrowroot', 37, 2.18, 80.66),
(188, 'East', 'New York', 'Cookies', 'Chocolate Chip', 40, 1.87, 74.8),
(189, 'West', 'San Diego', 'Bars', 'Bran', 26, 1.87, 48.62),
(190, 'East', 'Boston', 'Bars', 'Banana', 22, 2.27, 49.94),
(191, 'East', 'Boston', 'Cookies', 'Chocolate Chip', 32, 1.87, 59.84),
(192, 'East', 'Boston', 'Crackers', 'Whole Wheat', 23, 3.49, 80.27),
(193, 'West', 'Los Angeles', 'Cookies', 'Arrowroot', 20, 2.18, 43.6),
(194, 'West', 'Los Angeles', 'Cookies', 'Chocolate Chip', 64, 1.87, 119.68),
(195, 'East', 'New York', 'Bars', 'Carrot', 71, 1.77, 125.67),
(196, 'West', 'San Diego', 'Cookies', 'Arrowroot', 90, 2.18, 196.2),
(197, 'West', 'San Diego', 'Cookies', 'Oatmeal Raisin', 38, 2.84, 107.92),
(198, 'East', 'Boston', 'Bars', 'Carrot', 55, 1.77, 97.35),
(199, 'East', 'Boston', 'Snacks', 'Pretzels', 22, 3.15, 69.3),
(200, 'West', 'Los Angeles', 'Bars', 'Carrot', 34, 1.77, 60.18),
(201, 'East', 'New York', 'Bars', 'Bran', 39, 1.87, 72.93),
(202, 'East', 'New York', 'Cookies', 'Oatmeal Raisin', 41, 2.84, 116.44),
(203, 'West', 'San Diego', 'Bars', 'Carrot', 41, 1.77, 72.57),
(204, 'East', 'Boston', 'Cookies', 'Arrowroot', 136, 2.18, 296.48),
(205, 'East', 'Boston', 'Bars', 'Carrot', 25, 1.77, 44.25),
(206, 'East', 'Boston', 'Snacks', 'Pretzels', 26, 3.15, 81.9),
(207, 'West', 'Los Angeles', 'Bars', 'Bran', 50, 1.87, 93.5),
(208, 'West', 'Los Angeles', 'Cookies', 'Oatmeal Raisin', 79, 2.84, 224.36),
(209, 'East', 'New York', 'Bars', 'Carrot', 30, 1.77, 53.1),
(210, 'East', 'New York', 'Snacks', 'Potato Chips', 20, 1.68, 33.6),
(211, 'West', 'San Diego', 'Bars', 'Carrot', 49, 1.77, 86.73),
(212, 'East', 'Boston', 'Cookies', 'Arrowroot', 40, 2.18, 87.2),
(213, 'East', 'Boston', 'Bars', 'Carrot', 31, 1.77, 54.87),
(214, 'East', 'Boston', 'Snacks', 'Pretzels', 21, 3.15, 66.15),
(215, 'West', 'Los Angeles', 'Bars', 'Bran', 43, 1.87, 80.41),
(216, 'West', 'Los Angeles', 'Cookies', 'Oatmeal Raisin', 47, 2.84, 133.48),
(217, 'East', 'New York', 'Cookies', 'Arrowroot', 175, 2.18, 381.5),
(218, 'East', 'New York', 'Cookies', 'Chocolate Chip', 23, 1.87, 43.01),
(219, 'West', 'San Diego', 'Bars', 'Carrot', 40, 1.77, 70.8),
(220, 'East', 'Boston', 'Cookies', 'Arrowroot', 87, 2.18, 189.66),
(221, 'East', 'Boston', 'Bars', 'Carrot', 43, 1.77, 76.11),
(222, 'East', 'Boston', 'Crackers', 'Whole Wheat', 30, 3.49, 104.7),
(223, 'West', 'Los Angeles', 'Bars', 'Carrot', 35, 1.77, 61.95),
(224, 'East', 'New York', 'Bars', 'Bran', 57, 1.87, 106.59),
(225, 'East', 'New York', 'Snacks', 'Potato Chips', 25, 1.68, 42),
(226, 'West', 'San Diego', 'Cookies', 'Chocolate Chip', 24, 1.87, 44.88),
(227, 'East', 'Boston', 'Bars', 'Bran', 83, 1.87, 155.21),
(228, 'East', 'Boston', 'Cookies', 'Oatmeal Raisin', 124, 2.84, 352.16),
(229, 'West', 'Los Angeles', 'Bars', 'Carrot', 137, 1.77, 242.49),
(230, 'East', 'New York', 'Cookies', 'Arrowroot', 146, 2.18, 318.28),
(231, 'East', 'New York', 'Cookies', 'Chocolate Chip', 34, 1.87, 63.58),
(232, 'West', 'San Diego', 'Bars', 'Carrot', 20, 1.77, 35.4),
(233, 'East', 'Boston', 'Cookies', 'Arrowroot', 139, 2.18, 303.02),
(234, 'East', 'Boston', 'Cookies', 'Chocolate Chip', 211, 1.87, 394.57),
(235, 'East', 'Boston', 'Crackers', 'Whole Wheat', 20, 3.49, 69.8),
(236, 'West', 'Los Angeles', 'Bars', 'Bran', 42, 1.87, 78.54),
(237, 'West', 'Los Angeles', 'Cookies', 'Oatmeal Raisin', 100, 2.84, 284),
(238, 'East', 'New York', 'Bars', 'Carrot', 38, 1.77, 67.26),
(239, 'East', 'New York', 'Crackers', 'Whole Wheat', 25, 3.49, 87.25),
(240, 'West', 'San Diego', 'Cookies', 'Chocolate Chip', 96, 1.87, 179.52),
(241, 'East', 'Boston', 'Cookies', 'Arrowroot', 34, 2.18, 74.12),
(242, 'East', 'Boston', 'Cookies', 'Chocolate Chip', 245, 1.87, 458.15),
(243, 'East', 'Boston', 'Crackers', 'Whole Wheat', 30, 3.49, 104.7),
(244, 'West', 'Los Angeles', 'Bars', 'Bran', 30, 1.87, 56.1),
(245, 'West', 'Los Angeles', 'Cookies', 'Oatmeal Raisin', 44, 2.84, 124.96);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `admin` tinyint(4) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `admin`) VALUES
(4, 'Tony King', 'tking@gmx.co.uk', '1a1dc91c907325c69271ddf0c944bc72', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `food_orders`
--
ALTER TABLE `food_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `food_orders`
--
ALTER TABLE `food_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=246;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
