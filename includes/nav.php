<?php

/* 
  Navigation array, item properties are;
    page name
    display flag;
      null = always
      0 = when logged out
      1 = when logged in
*/

$navItems = array(
	'Home' => array('index.php',null),
	'Login' => array('login.php',0),
	'Restricted' => array('restricted.php',1),
	'Static' => array('static-page.php',null),
  'Logout' => array('login.php?logout=true',1)
);

?>
<!-- Navigation bar -->
<nav class="navbar navbar-expand-sm bg-dark navbar-dark justify-content-center">
  <ul class="navbar-nav">
  	<?php
    /* Loop through array display menu items as appropriate */
  	foreach ($navItems as $item => $data) {

      /* Check whether item should be displayed */
      if (($data[1] === null) || (!$loggedIn && !$data[1]) || ($loggedIn && $data[1])) {
      ?>
      <li class="nav-item">
        <a class="nav-link" href="<?=$data[0]; ?>"><?=$item; ?></a>
      </li>
    	<?php
      } // End if
  	} // End foreach
  	?>
  </ul>

</nav>
<?php
